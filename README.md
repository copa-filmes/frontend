# frontend

Um frontend PWA feito para escolher os melhores filmes.
Atualmente rodando em https://copa-filmes.victorperin.ninja

## Como rodar
Para rodar o projeto é só rodar os comandos:
```bash
$ npm install
$ npm start
```

Isso irá rodar o projeto em modo de desenvolvimento, qualquer alteração que você fizer no projeto será automaticamente atualizada.


### Rodar build
Você também pode rodar um script para criar o código estático do projeto. Assim é fácil fazer deploy para algum lugar. Para fazer isso, execute:
```
$ npm run build
```

## Como rodar os testes
Temos 2 tipos de testes:
  - __unitários__: servem para testar os componentes mais importantes e logicas
  - __integração__: verificar se todos os componentes estão se comunicando e se não vai travar o usuário de alguma maneira.

### Testes unitários
Para rodar os testes unitários, execute:
```bash
$ npm test
```

Se você estiver em um terminal, interface (exceção de ambientes de CI, ou algo parecido), você entrará no modo de desenvolvimento, cada alteração que você fizer no projeto, fazem eles rodarem novamente.

Já, em um ambiente de ci (como é o caso do CI do gitlab), os testes irão executar uma vez apenas.

### Testes de integração
Para rodar os testes de integração, execute:
```bash
$ npm run integration-test
```

Ao executar, ele iniciara o modo de desenvolvimento do now.sh (onde o front-end está hospedado). Com isso, tome cuidado para não ter outro projeto rodando na porta 3000.

Após iniciar esse modo e o build estiver concluido, ele começa a rodar o cypress, responsável pelos testes de integração da aplicação.

## Estrutura de arquivos
```bash
├── cypress # testes de integração do cypress
│   └── integration
│       └── UserHappyPath.spec.js
├── cypress.json # arquivo de configuração do cypress
├── package.json # arquivo base para projeto JavaScript contendo pacotes,
│                # scripts e outras configurações relacionadas ao
│                # ambiente JavaScript
├── build # pasta gerada automaticamente pelo build, com arquivos
│         # estáticos para serem colocados em produção (não mexa)
├── public # arquivos estáticos base (logos, pwa-manifest, robots, etc)
│   ├── favicon.ico
│   ├── index.html
│   ├── logo192.png
│   ├── logo512.png
│   ├── manifest.json
│   └── robots.txt
├── README.md # você está lendo esse arquivo
├── src # source dos arquivos do projeto
│   ├── App.test.tsx
│   ├── App.tsx # do componente principal
│   ├── Components # todos os componentes da aplicação organizados aqui
│   │   ├── CheckBoxContainer.tsx # junta todos os checkbox de filmes
│   │   ├── Header.test.tsx
│   │   ├── Header.tsx # header principal
│   │   ├── MovieCheckbox.test.tsx
│   │   ├── MovieCheckbox.tsx # checkbox de filmes (wraps simplecheckbox)
│   │   ├── SimpleCheckbox.tsx # componente de checkbox simples
│   │   └── __snapshots__ # snapshots do jest (não altere manualmente)
│   │       ├── Header.test.tsx.snap
│   │       └── MovieCheckbox.test.tsx.snap
│   ├── Contexts
│   │   ├── MoviesContextProvider.tsx
│   │   └── MoviesContext.tsx
│   ├── index.tsx # arquivo para montar o projeto no index.html
│   │             # em public,e registrar service worker
│   ├── pages # paginas para renderizar
│   │   ├── FinalResult.tsx # paginal de resultados
│   │   └── SelectionStep.tsx #pagina de seleção
│   ├── react-app-env.d.ts
│   ├── Routes.tsx # rotas, qual url renderiza que pagina.
│   └── serviceWorker.ts # bit.ly/what-is-a-service-worker
└── tsconfig.json # configurações do typescript
```

## Continous integration
Estamos usando o GitLab CI para construir a aplicação. Nele, temos 3 passos:
  - test
  - build
  - deploy

## tests
Aqui temos 2 etapas:
- test: roda testes unitários no jest
- integration_test: roda testes de integração no cypress

## build
O processo de build é responsável por checar se a pasta de build consegue ser gerada.

## deploy
É feito automaticamente pelo now.sh. Roda apenas no branch master e faz o deploy para a url do topo.
