import React from 'react'
import styled from 'styled-components'

type props = {
  isDisabled: boolean
  checked: boolean
  id: string
}

const StyledCheckbox = styled.div`
  input[type=checkbox] + label {
    display: flex;
    margin: 0.2em;
    cursor: pointer;
    padding: 0.2em;
  }

  input[type=checkbox] {
    display: none;
  }

  input[type=checkbox] + label:before {
    content: "✔";
    border: 0.1em solid #000;
    border-radius: 0.2em;
    width: 2em;
    height: 2em;
    display: flex;
    justify-content: center;
    align-items: center;
    margin-right: 0.7em;
    color: transparent;
    transition: .2s;
  }

  input[type=checkbox] + label:active:before {
    transform: scale(0);
  }

  input[type=checkbox]:checked + label:before {
    background-color: MediumSeaGreen;
    border-color: MediumSeaGreen;
    color: #fff;
  }

  input[type=checkbox]:disabled + label:before {
    transform: scale(1);
    border-color: #aaa;
    background-color: #aaa;
  }

  input[type=checkbox]:checked:disabled + label:before {
    transform: scale(1);
    background-color: #bfb;
    border-color: #bfb;
  }
`
type ExecutionEvent = React.MouseEvent | React.ChangeEvent
const preventExecution = (event: ExecutionEvent) => event.preventDefault()

const SimpleCheckbox: React.FC<props> = ({checked, id, isDisabled, children}) => 
  <StyledCheckbox>
    <input id={id} type="checkbox" checked={checked} onClick={preventExecution} onChange={preventExecution} disabled={isDisabled} />
    <label htmlFor={id} onClick={preventExecution}>{children}</label>
  </StyledCheckbox>

export default SimpleCheckbox
