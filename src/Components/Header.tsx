import * as React from 'react'
import styled from 'styled-components'

interface ExpectedProps {
  mainText: string
  description: string
}

const StyledHeader = styled.header`
  background-color: #6E6E6E;
  text-align: center;
  color: #FFFFFF;
  margin-top: 30px;
  margin-bottom: 10px;
  padding: 70px 0;

  h5 {
    color: #A6A6A6;
    text-transform: uppercase;
  }
`

const Header: React.StatelessComponent<ExpectedProps> = ({ mainText, description }) =>
  <StyledHeader>
    <h5>Campeonato de filmes</h5>
    <h1>{mainText}</h1>
    <p>{description}</p>
  </StyledHeader>

export default Header
