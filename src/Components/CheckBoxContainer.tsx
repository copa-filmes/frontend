import React, { useEffect, useContext } from 'react'
import styled from 'styled-components'
import MovieCheckbox, { Movie } from './MovieCheckbox'
import MoviesContext from '../Contexts/MoviesContext'

const StyledContainer = styled.div`
    margin: auto;
    padding: 10px;
    max-width: 1440px;
    
    display: grid;
    --columns: 4;
    grid-template-columns: repeat(var(--columns), 1fr);
    grid-gap: 10px;

    @media (max-width: 1170px) { --columns: 3 }
    @media (max-width: 768px)  { --columns: 2 }
    @media (max-width: 450px)  { --columns: 1 }
`

const MoviesMapper = (movie: Movie) => {
  const { toggleMovie } = useContext(MoviesContext)
  const onToggle = () => toggleMovie(movie)

  return <MovieCheckbox key={movie.id} movie={movie} onToggle={onToggle}/>
}

const CheckboxContainer = () => {
  const [movies, setMovies] = React.useState([]);

  useEffect( () => {
    fetch('https://api.copa-filmes.victorperin.ninja/filmes', {
      method: 'GET',
      headers: { 'Accept': 'application/json' }
    })
      .then(response =>
          !response.ok ?
            Promise.reject(response) :
            response.json()
      )
      .then( (data) => setMovies(data) )
      .catch( () => setMovies([]) )
  }, [])

  return <StyledContainer >
    {movies.map(MoviesMapper)}
  </StyledContainer>
}

export default CheckboxContainer
