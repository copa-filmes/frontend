import React from 'react'
import styled from 'styled-components'
import Checkbox from './SimpleCheckbox'
import { useMoviesContext } from '../Contexts/MoviesContext'

export interface Movie {
  id: string
  titulo: string
  ano: number
  nota: number
}

interface ExpectedProps {
  movie: Movie
  onToggle: Function
}

// Flex is 21%
const StyledMovieCheckbox = styled.div`
  min-width: 150px; 
  background-color: #ffffff;
  cursor: pointer;
  display: inline-block;
  vertical-align: middle;

  padding: 35px 25px;
  border-radius: 3px;
  margin: 7px;

  div {
    display: inline-block;
    vertical-align: middle;

    small {
      color: #858585;
      font-size: 75%;
    }

    span {
      flex: 1;
      display: inline-block;
      font-style: italic;
    }
  }
`

const MovieCheckbox: React.StatelessComponent<ExpectedProps> = ({movie, onToggle}) => {
  const {movies: selectedMovies} = useMoviesContext()
  const isChecked = selectedMovies.map( movie => movie.id ).includes(movie.id)

  const isDisabled = selectedMovies.length >= 8 && !isChecked
  

  const toggleCheck = () => {
    if(isDisabled) return;
    onToggle()
  }

  return <StyledMovieCheckbox onClick={toggleCheck}>
    <Checkbox id={movie.id} checked={isChecked} isDisabled={isDisabled}  >
      <span>
        {movie.titulo}<br/>
        <small>{movie.ano}</small>
      </span>
    </Checkbox>
      
  </StyledMovieCheckbox>
}

export default MovieCheckbox
