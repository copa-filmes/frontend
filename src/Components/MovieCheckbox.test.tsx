import * as React from "react"
import { create, act } from "react-test-renderer"
import MovieCheckbox from "./MovieCheckbox"
import * as MoviesContext from '../Contexts/MoviesContext'

describe("MovieCheckbox", () => {
  it("Should create MovieCheckbox with title Biru and releaseYear 9000",() => {
    const contextValues = { movies: [], toggleMovie: () => {} }

    jest
      .spyOn(MoviesContext, 'useMoviesContext')
      .mockImplementation( () => contextValues )

    const movie = { id: 'a', titulo: 'Biru', ano: 9000, nota: 10 }
    const component = create(<MovieCheckbox movie={movie} onToggle={() => {}} />);

    expect(component.toJSON()).toMatchSnapshot()
  })

  it("Should check checkbox on click", () => {
    const movie = { id: 'a', titulo: 'Biru', ano: 9000, nota: 10 }
    const contextValues = { movies: [movie], toggleMovie: () => {} }

    jest
      .spyOn(MoviesContext, 'useMoviesContext')
      .mockImplementation( () => contextValues )

    const component = create(<MovieCheckbox movie={movie} onToggle={() => {}}/>)

    act( () => component.root.findByType('div').props.onClick() )

    expect(component.root.findByType('input').props.checked).toBeTruthy()
  })

  it("Should uncheck checkbox on second click", () => {
    const movie = { id: 'a', titulo: 'Biru', ano: 9000, nota: 10 }
    const contextValues = { movies: [], toggleMovie: () => {} }

    jest
      .spyOn(MoviesContext, 'useMoviesContext')
      .mockImplementation( () => contextValues )


    const component = create(<MovieCheckbox movie={movie} onToggle={() => {}}/>)

    act( () => component.root.findByType('div').props.onClick() )
    act( () => component.root.findByType('div').props.onClick() )

    expect(component.root.findByType('input').props.checked).toBeFalsy()
  })
})
