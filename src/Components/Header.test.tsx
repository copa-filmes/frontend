import * as React from "react"
import { create } from "react-test-renderer"
import Header from "./Header"

describe("Header", () => {
  it("Should create header with mainText Biru and description Laibe",() => {
    const component = create(<Header mainText='Biru' description='Laibe'/>)

    expect(component.toJSON()).toMatchSnapshot()
  })
})
