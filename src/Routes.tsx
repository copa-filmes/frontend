import * as React from "react"
import { BrowserRouter, Switch, Route } from "react-router-dom"
import SelectionStep from "./pages/SelectionStep"
import FinalResult from "./pages/FinalResult"

const RoutesList = () =>
  <div>
    <Route path="/final-result" exact={true}  component={FinalResult} />
    <Route path="/" exact={true}  component={SelectionStep} />
  </div>

const RouteFragmentRender = () =>
  <React.Fragment>
    <RoutesList/>
  </React.Fragment>

export const Routes = () => 
  <BrowserRouter>
    <Switch>
      {/* <Route path="/login" component={LoginView} /> */}
      <Route path="/" render={RouteFragmentRender} />
    </Switch>
  </BrowserRouter>
