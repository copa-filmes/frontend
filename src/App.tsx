import React from "react"
import { createGlobalStyle } from "styled-components"
import { Routes } from "./Routes"
import MoviesContextProvider from './Contexts/MoviesContextProvider'

const GlobalStyle = createGlobalStyle`
body {
  background-color: #A8A8A8;
  margin: 0;
}

* {
  box-sizing: border-box;
  font-family: 'Ubuntu', sans-serif;
}

*:focus {
  outline: 0;
}

a {
  color: #0d0d0d;
  text-decoration: none;
}
`;

const App = () =>
  <div>
    <GlobalStyle />
    <MoviesContextProvider>
      <Routes />
    </MoviesContextProvider>
  </div>

export default App
