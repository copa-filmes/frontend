import React, { useEffect, useState, useContext } from "react"
import Header from '../Components/Header'
import styled from 'styled-components'
import MoviesContext from '../Contexts/MoviesContext'

const mainText = 'Resultado final'
const headerDescription = 'Veja o resultado final do Campeonato de filmes de forma simples e rápida'

const SyledChampion = styled.div`
  margin-top: 10px;
  fond-size: 1.5em;
  div.position {
    display: inline-block;
    width: 150px;
    background-color: #6E6E6E;
    color: #fff;
    padding: 20px 30px;
  }

  div.titulo {
    display: inline-block;
    width: 50%;
    background-color: #fff;
    padding: 20px 30px;
  }
`

const FinalResult: React.StatelessComponent = () => {
  const {movies: selectedMovies} = useContext(MoviesContext)
  const [finalists, setFinalists] = useState([]);

  useEffect( () => {
    fetch("https://api.copa-filmes.victorperin.ninja/copa", {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(selectedMovies)
    })
      .then(response =>
          !response.ok ?
            Promise.reject(response) :
            response.json()
      )
      .then( (data) => setFinalists(data) )
      .catch( () => setFinalists([]) )
  }, [selectedMovies])


  return <div>
    <Header mainText={mainText} description={headerDescription} />
    { finalists.map(({ titulo }, index) =>
      <SyledChampion>
        <div className="position" >{index+1}º</div>
        <div className="titulo" >{titulo} {index ? 'Vice-Campeão' : 'Campeão'}</div>
      </SyledChampion>
    )}
  </div>
}
  

export default FinalResult
