import React, { useContext } from "react"
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import Header from "../Components/Header"
import CheckboxContainer from '../Components/CheckBoxContainer'
import MoviesContext from '../Contexts/MoviesContext'

const mainText = 'Fase de seleção'
const headerDescription = 'Selecione 8 filmes que você deseja que entrem na competição e, depois, pressione o botão Gerar Meu Campeonato para proseguir.'

const StyledMenu = styled.div`
  position: relative;
  margin: auto;
  padding: 10px 20px;
  max-width: 1440px;

  h2 {
    color: #fff;
  }

  button {
    right: 0;
    /* padding: 40px 20px; */
    position: absolute;
    top: 50%;
    transform: translate(-20px, -50%);
    max-width: 320px;
    min-height: 70px;
    width: 100%;
    background: MediumSeaGreen;
    color: #fff;
    font-size: 1.2em;
    font-weight: bold;
    text-transform: uppercase;    
    border: none;
    border-radius: 3px;
    transition: .2s;
  }

  button:disabled {
    background: #343434;
  }

  button:active {
    background-color: DarkSeaGreen;
  }
`
const Menu = () => {
  const { movies: selectedMovies } = useContext(MoviesContext)
  const isGerarDisabled = selectedMovies.length < 8

  return <StyledMenu>
    <h2> Selecionados <br/> {selectedMovies.length} de 8 filmes </h2>
    <Link to="/final-result">
      <button id="submit-copa" disabled={isGerarDisabled}>gerar meu campeonato</button>
    </Link>
  </StyledMenu>
}

const Home: React.StatelessComponent = () =>
  <div>
    <Header mainText={mainText} description={headerDescription} />
      <Menu/>
    <CheckboxContainer />
  </div>

export default Home
