import React, { useContext } from "react";
import { Movie } from '../Components/MovieCheckbox'

type MoviesContext = {
  movies: Movie[],
  toggleMovie: Function,
}

const MoviesContext= React.createContext({} as MoviesContext);

const { Provider, Consumer } = MoviesContext;

const useMoviesContext = () => useContext(MoviesContext)

export { Provider, Consumer, useMoviesContext };
export default MoviesContext;
