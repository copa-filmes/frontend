import React, { useState } from 'react'
import MoviesContext from './MoviesContext'
import { Movie } from '../Components/MovieCheckbox'
import xor from 'lodash.xor'

const MoviesContextProvider: React.FunctionComponent = ({ children }) => {

  const [movies, setMovies]: [Movie[], Function] = useState([])

  const context = {
    movies,
    toggleMovie: (movie: Movie) => {
      const selectedMovie = movies.find((x) => x.id === movie.id)
      setMovies( xor(movies, [selectedMovie || movie]) )
    }
  }

  return (
    <MoviesContext.Provider value={context}>
      {children}
    </MoviesContext.Provider>
  )
}

export default MoviesContextProvider
