context('SelectionStep page', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000')
  })

  it('Should get the first 8 items and get Vingadores and Os Incriveis as result', () => {
    const itemsToClick = [1,2,3,4,5,6,7,8]

    itemsToClick.forEach( (item) => {
      const element = `#root > div > div > div > div.sc-bxivhb.fIaHMY > div:nth-child(${item})`
      cy.get(element).click()
    })

    cy.get('#submit-copa').click()

    cy.get('#root > div > div > div > div:nth-child(2) > div.titulo')
      .should('contain', 'Vingadores: Guerra Infinita')
      .should('contain', 'Campeão')

    cy.get('#root > div > div > div > div:nth-child(3) > div.titulo')
      .should('contain', 'Os Incríveis 2')
      .should('contain', 'Vice-Campeão')
  })
})
